import asyncio
import aio_pika
import json
from functools import partial


def multiply(x, y):
    return x * y

async def message_process(exchange, message):
    async with message.process():
        print(message.body)
        parsed_msg = json.loads(message.body)
        result = multiply(x=parsed_msg['x'], y=parsed_msg['y'])
        await exchange.publish(
            aio_pika.Message(
                body=result,
                correlation_id=message.correlation_id
            ),
            routing_key=message.reply_to,
        )
        print('message processed')


async def main(loop):
    connection = await aio_pika.connect_robust(
        'amqp://guest:guest@127.0.0.1:5672/',
        loop=loop
    )

    request_queue_name = 'multiply-request-queue'

    async with connection:
        channel = await connection.channel()

        queue = await channel.declare_queue(request_queue_name,
                                            auto_delete=True)
        await queue.consume(partial(
            message_process, channel.default_exchange)
        )


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.create_task(main(loop))
    loop.run_forever()
