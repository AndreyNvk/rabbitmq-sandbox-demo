import asyncio
import json
import uuid
import aio_pika


async def remote_procedure_call( procedure_name, x, y):
    connection = await aio_pika.connect_robust(
        'amqp://guest:guest@127.0.0.1:5672/',
        loop=asyncio.get_event_loop()
    )
    async with connection:
        async with connection.channel() as channel:
            response_queue = await channel.declare_queue(
                f'{procedure_name}-response-queue',
                auto_delete=True)

            routing_key = f'{procedure_name}-request-queue'
            await channel.default_exchange.publish(
                aio_pika.Message(
                    body=json.dumps({'x': x, 'y': y}).encode(),
                    reply_to=response_queue,
                    correlation_id=str(uuid.uuid4())
                ),
                routing_key=routing_key,
            )
            async with response_queue.iterator() as queue_iter:
                async for message in queue_iter:
                    async with message.process():
                        return message.body


async def main():
    for i in range(1000):
        print(await remote_procedure_call('multiply', 2, i))


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    loop.close()
