import asyncio
from aio_pika import connect_robust
from aio_pika.patterns import RPC


async def multiply(*, x, y):
    print(f'{x} * {y} is here, okay or not???')
    return x * y


async def summarize(*, x, y):
    print(f'{x} + {y} is here, okay or not???')
    return x + y


async def main():
    connection = await connect_robust(
        "amqp://guest:guest@127.0.0.1:5672/",
        client_properties={"connection_name": "callee"},
        loop=asyncio.get_event_loop()
    )

    # Creating channel
    channel = await connection.channel()

    rpc = await RPC.create(channel)
    await rpc.register("wtf_omg", multiply, auto_delete=True)
    await rpc.register("i_dont_care", summarize, auto_delete=True)

    return connection


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    connection = loop.run_until_complete(main())

    try:
        loop.run_forever()
    finally:
        loop.run_until_complete(connection.close())
        loop.shutdown_asyncgens()