import sys
import asyncio
from aio_pika import connect, Message, DeliveryMode, ExchangeType


async def main(loop):
    connection = await connect(
        'amqp://guest:guest@127.0.0.1:5672/',
        loop=loop
    )
    async with connection:
        channel = await connection.channel()
        exchange = await channel.declare_exchange(
            'fanout_exchange_example', ExchangeType.FANOUT
        )

        message_body = b" ".join(
            arg.encode() for arg in sys.argv[1:]) or b"Hello World!"
        message = Message(
            message_body,
            delivery_mode=DeliveryMode.PERSISTENT
        )
        await exchange.publish(message, routing_key='amq.gen-76XE5eYbFV2e0MF_WcNPTQ')
        print(f'Sent {message_body}')


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
