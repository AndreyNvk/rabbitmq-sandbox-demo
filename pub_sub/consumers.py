import asyncio
import typing
from aio_pika import connect, IncomingMessage, ExchangeType


async def on_message_consumer_a(message: IncomingMessage):
    async with message.process():
        print(f'Consumer A received {message.body}')


async def on_message_consumer_b(message: IncomingMessage):
    async with message.process():
        print(f'Consumer B received {message.body}')


async def main(on_message: typing.Callable):
    connection = await connect(
        'amqp://guest:guest@127.0.0.1:5672/',
        loop=asyncio.get_event_loop(),
    )


    channel = await connection.channel()
    await channel.set_qos(prefetch_count=1)

    logs_exchange = await channel.declare_exchange(
        'fanout_exchange_example', ExchangeType.FANOUT
    )
    queue = await channel.declare_queue(exclusive=True)

    await queue.bind(logs_exchange)
    await queue.consume(on_message)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.create_task(main(on_message_consumer_a))
    loop.create_task(main(on_message_consumer_b))

    loop.run_forever()
