## rabbit sandbox demo

Some examples of using Rabbit MQ

### Run

```bash
pip install -r requirements.txt
```

Start Rabbit:
```bash
docker-compose up
```

Then go to one of the dirs and first run consumer, then producer

### Rabbit

[Web GUI is here](http://localhost:15672) `guest` - `guest`
